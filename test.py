# 导入工具包
from imutils import contours
import numpy as np
import argparse
import cv2 as cv
import myutils

# 设置参数
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="path to input image")
ap.add_argument("-t", "--template", required=True,
                help="path to template OCR-A image")
args = vars(ap.parse_args())

# 指定信用卡类型
FIRST_NUMBER = {
    "3": "American Express",
    "4": "Visa",
    "5": "MasterCard",
    "6": "Discover Card"
}


# 绘图展示
def cv_show(name, image):
    cv.imshow(name, image)
    cv.waitKey(0)
    cv.destroyAllWindows()


# 读取模板图像
img = cv.imread(args['template'])
cv_show('template', img)
# 灰度图
ref = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv_show('gray', ref)
# 二值图像
# 为什么[1]?
ref = cv.threshold(ref, 10, 255, cv.THRESH_BINARY_INV)[1]
cv_show('ref', ref)

# 计算轮廓
# cv2.findContours()函数接受的参数为二值图，即黑白的（不是灰度图）,cv2.RETR_EXTERNAL只检测外轮廓，cv2.CHAIN_APPROX_SIMPLE只保留终点坐标
# 返回的list中每个元素都是图像中的一个轮廓

refCnts, hierarchy = cv.findContours(ref.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
cv.drawContours(img, refCnts, -1, (0, 0, 255), 3)
# 显示轮廓
cv_show('contours', img)
print(np.array(refCnts).shape)
# 为什么要加[0]
refCnts = myutils.sort_contours(refCnts, method='left-to-right')[0]
digits = {}

# 遍历每一个轮廓
for (i, c) in enumerate(refCnts):
    # 计算外接矩形并且resize成合适大小
    (x, y, w, h) = cv.boundingRect(c)
    roi = ref[y:y + h, x:x + w]
    roi = cv.resize(roi, (57, 88))

    # 每个数字对应一个模板
    digits[i] = roi

cv_show('11',digits[1])